package az.module2finalproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Module2FinalProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(Module2FinalProjectApplication.class, args);
    }

}
