package az.module2finalproject.service;

import az.module2finalproject.entity.Task;
import az.module2finalproject.entity.User;
import az.module2finalproject.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;

    private final UserService userService;

    public Task createTask(String description, String username) {
        User user = userService.findByUsername(username);
        Task task = new Task();
        task.setDescription(description);
        task.setCompleted(false);
        task.setUser(user);
        return taskRepository.save(task);
    }

    public List<Task> getTasks(String username) {
        User user = userService.findByUsername(username);
        return taskRepository.findByUser(user);
    }

    public Task getTaskById(Long id, String username) {
        User user = userService.findByUsername(username);
        Optional<Task> taskOpt = taskRepository.findById(id);
        if (taskOpt.isPresent() && taskOpt.get().getUser().equals(user)) {
            return taskOpt.get();
        }
        return null;
    }

    public Task updateTask(Long id, Task updatedTask, String username) {
        User user = userService.findByUsername(username);
        Optional<Task> taskOpt = taskRepository.findById(id);
        if (taskOpt.isPresent() && taskOpt.get().getUser().equals(user)) {
            Task task = taskOpt.get();
            task.setDescription(updatedTask.getDescription());
            task.setCompleted(updatedTask.isCompleted());
            return taskRepository.save(task);
        }
        return null;
    }

    public boolean deleteTask(Long id, String username) {
        User user = userService.findByUsername(username);
        Optional<Task> taskOpt = taskRepository.findById(id);
        if (taskOpt.isPresent() && taskOpt.get().getUser().equals(user)) {
            taskRepository.delete(taskOpt.get());
            return true;
        }
        return false;
    }
}
