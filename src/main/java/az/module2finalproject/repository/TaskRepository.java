package az.module2finalproject.repository;

import az.module2finalproject.entity.Task;
import az.module2finalproject.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task,Long> {

    List<Task> findByUser(User user);
}
